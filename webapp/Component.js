sap.ui.define(["sap/suite/ui/generic/template/lib/AppComponent"], function (AppComponent) {
	return AppComponent.extend("sap.test.testsubj.Component", {
		metadata: {
			"manifest": "json"
		}
	});
});